const express = require("express");
const router = express.Router();

const emailController = require("../controllers/emailController");

router.get("/", emailController.get_email);
router.get("/", emailController.email_get);
router.get("/", emailController.get_emails);
router.post("/", emailController.email_post);

module.exports = router;
