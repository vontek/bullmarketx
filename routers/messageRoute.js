const express = require("express");
const router = express.Router();

const messageController = require("../controllers/messageController");

router.get("/message/:id", messageController.get_message);
router.get("/message", messageController.get_messages);
router.post("/message", messageController.post_message);
router.put("/message", messageController.update_message);
router.delete("/message", messageController.delete_message);

module.exports = router;
