const express = require("express");
const router = express.Router();
const upload = require("../config/multer");
const walletController = require("../controllers/walletController");

router.get("/wallets/:id", walletController.getWallet);
router.get("/wallets", walletController.getAllWallets);
router.delete("/wallets/:id", walletController.deleteWallet);
router.put(
  "/wallets/:id",
  upload.single("qr_code"),
  walletController.updateWallet
);
router.post(
  "/wallets",
  upload.single("qr_code"),
  walletController.createWallet
);

module.exports = router;
