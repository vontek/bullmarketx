const express = require("express");
const router = express.Router();
const withdrawalController = require("../controllers/withdrawalController");

router.post("/withdrawals", withdrawalController.createWithdrawal);
router.get("/withdrawals", withdrawalController.getAllWithdrawals);

module.exports = router;
