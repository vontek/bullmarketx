const express = require("express");
const router = express.Router();
const upload = require("../config/multer");
const profileController = require("../controllers/profileController");

router.get("/profile", profileController.getProfile);
router.post(
  "/profile",
  upload.single("profilePicture"),
  profileController.createProfile
);
router.put("/profile/:id", profileController.updateProfile);
router.get("/profile/:id", profileController.getProfileById);
router.delete("/profile/:id", profileController.deleteProfile);

module.exports = router;
