const express = require("express");
const router = express.Router();

// admin routes
const User = require("../../models/User");
const Plan = require("../../models/Plan");
const Wallet = require("../../models/Wallet");
const Message = require("../../models/Message");
const Profile = require("../../models/Profile");
const Withdrawal = require("../../models/Withdrawal");
const Transaction = require("../../models/Transaction");
const { requireRole } = require("../../middleware/auth");
const { protectAdminRoute } = require("../../middleware/auth");

// router.all("/admin/*", requireRole("admin"));
router.get("/admin/dashboard", requireRole("admin"), async (req, res) => {
  try {
    const transaction = await Transaction.find({}).populate("user").exec();
    const withdrawal = await Withdrawal.find({}).populate("user").exec();
    // console.log(withdrawal);
    res.render(__dirname + "../../../views/admin/dashboard", {
      data: transaction,
      withdrawal: withdrawal,
    });
  } catch (err) {
    console.error(err);
    // Handle the error appropriately (e.g., send an error response).
    res.status(500).send("An error occurred");
  }
});

router.get(
  "/admin/active-users",
  requireRole("admin"),
  // protectAdminRoute,
  async (req, res) => {
    try {
      const profile = await Profile.find({}).populate("user").exec();
      res.render(__dirname + "../../../views/admin/active-users", {
        data: profile,
      });
    } catch (err) {
      console.error(err);
      // Handle the error appropriately (e.g., send an error response).
      res.status(500).send("An error occurred");
    }
  }
);

router.get(
  "/admin/withdrawal",
  requireRole("admin"),
  protectAdminRoute,
  (req, res) => {
    res.render(__dirname + "../../../views/admin/withdrawal");
  }
);

router.get(
  "/admin/wallet",
  requireRole("admin"),
  protectAdminRoute,
  (req, res) => {
    res.render(__dirname + "../../../views/admin/wallet");
  }
);

router.get("/admin/users", requireRole("admin"), async (req, res) => {
  try {
    const user = await User.find({}).exec();
    res.render(__dirname + "../../../views/admin/users", { data: user });
  } catch (err) {
    console.error(err);
    // Handle the error appropriately (e.g., send an error response).
  }
});

router.get(
  "/admin/transactions",
  requireRole("admin"),
  protectAdminRoute,
  (req, res) => {
    res.render(__dirname + "../../../views/admin/transactions");
  }
);

router.get(
  "/admin/settings",
  requireRole("admin"),
  protectAdminRoute,
  (req, res) => {
    res.render(__dirname + "../../../views/admin/settings");
  }
);

// router.get("/admin/register", requireRole("admin"), protectAdminRoute,   (req, res) => {
//     res.render(__dirname + "../../../views/admin/register");
// });

router.get(
  "/admin/profile",
  requireRole("admin"),
  protectAdminRoute,
  (req, res) => {
    res.render(__dirname + "../../../views/admin/profile");
  }
);

router.get("/admin/plan-list", requireRole("admin"), async (req, res) => {
  try {
    const plan = await Plan.find({}).exec();
    res.render(__dirname + "../../../views/admin/plan-list", { data: plan });
  } catch (err) {
    console.error(err);
    // Handle the error appropriately (e.g., send an error response).
    res.status(500).send("An error occurred");
  }
});

router.get(
  "/admin/manage-admin",
  requireRole("admin"),
  protectAdminRoute,
  (req, res) => {
    res.render(__dirname + "../../../views/admin/manage-admin");
  }
);

// router.get("/admin/login", requireRole("admin"), protectAdminRoute,   (req, res) => {
//     res.render(__dirname + "../../../views/admin/login");
// });

router.get(
  "/admin/investment-history",
  requireRole("admin"),
  protectAdminRoute,
  (req, res) => {
    res.render(__dirname + "../../../views/admin/investment-history");
  }
);

router.get(
  "/admin/edit-user",
  requireRole("admin"),
  protectAdminRoute,
  (req, res) => {
    res.render(__dirname + "../../../views/admin/edit-user");
  }
);

router.get(
  "/admin/edit-plan",
  requireRole("admin"),
  protectAdminRoute,
  (req, res) => {
    res.render(__dirname + "../../../views/admin/edit-plan");
  }
);

router.get(
  "/admin/edit-admin",
  requireRole("admin"),
  protectAdminRoute,
  (req, res) => {
    res.render(__dirname + "../../../views/admin/edit-admin");
  }
);

router.get(
  "/admin/deposit",
  requireRole("admin"),
  protectAdminRoute,
  (req, res) => {
    res.render(__dirname + "../../../views/admin/deposit");
  }
);

router.get("/admin/deactive-users", (req, res) => {
  res.render(__dirname + "../../../views/admin/deactive-users");
});

router.get(
  "/admin/contact-messages",
  requireRole("admin"),
  async (req, res) => {
    try {
      const message = await Message.find({}).exec();
      res.render(__dirname + "../../../views/admin/contact-messages", {
        data: message,
      });
    } catch (err) {
      console.error(err);
      // Handle the error appropriately (e.g., send an error response).
      res.status(500).send("An error occurred");
    }
  }
);

router.get(
  "/admin/tickets-list",
  requireRole("admin"),
  protectAdminRoute,
  (req, res) => {
    res.render(__dirname + "../../../views/admin/tickets-list");
  }
);

router.get(
  "/admin/company-settings",
  requireRole("admin"),
  protectAdminRoute,
  (req, res) => {
    res.render(__dirname + "../../../views/admin/company-settings");
  }
);

router.get(
  "/admin/bank-account",
  requireRole("admin"),
  protectAdminRoute,
  (req, res) => {
    res.render(__dirname + "../../../views/admin/bank-account");
  }
);

router.get(
  "/admin/admin",
  requireRole("admin"),
  protectAdminRoute,
  (req, res) => {
    res.render(__dirname + "../../../views/admin/admin");
  }
);

router.get(
  "/admin/add-plan",
  requireRole("admin"),
  protectAdminRoute,
  (req, res) => {
    res.render(__dirname + "../../../views/admin/add-plan");
  }
);

router.get(
  "/admin/add-payment",
  requireRole("admin"),
  protectAdminRoute,
  (req, res) => {
    res.render(__dirname + "../../../views/admin/add-payment");
  }
);

router.get("/admin/payment-list", requireRole("admin"), async (req, res) => {
  try {
    const wallet = await Wallet.find({}).populate("qr_code");
    // console.log(wallet);
    res.render(__dirname + "../../../views/admin/payment-list", {
      data: wallet,
    });
  } catch (err) {
    console.error(err);
    // Handle the error appropriately (e.g., send an error response).
    res.status(500).send("An error occurred");
  }
});

module.exports = router;
