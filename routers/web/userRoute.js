const express = require("express");
const { getTwoFactor } = require("../../controllers/twoFactorController");
const { protectRoute } = require("../../middleware/auth");
const { verify2Factor } = require("../../middleware/2FA");
const fetchUserProfile = require("../../middleware/fetchUserProfile");

const router = express.Router();

const Plan = require("../../models/Plan");
const Wallet = require("../../models/Wallet");
const Profile = require("../../models/Profile");
const User = require("../../models/User");

router.all("/user/*", protectRoute, verify2Factor, fetchUserProfile);
router.get("/user/dashboard", async (req, res) => {
  try {
    const userId = req.user.id; // Get the logged-in user's ID.

    // Fetch the user data from your data source (e.g., database).
    const user = await User.findById(userId);

    if (!user) {
      return res.status(404).send("User not found");
    }

    // Fetch the user's profile data (replace with your actual method to fetch the profile).
    const userProfile = await Profile.findOne({ user: userId }).exec();

    // console.log(userProfile);
    // Render the EJS template and pass both user and userProfile data.
    res.render(__dirname + "../../../views/user/dashboard", {
      user: user,
      userProfile: userProfile, // Pass the userProfile data to the template.
    });
  } catch (err) {
    console.error(err);
    res.status(500).send("Internal Server Error");
  }
});

// user routes
router.get("/user/add-fund", async (req, res) => {
  try {
    const userId = req.user.id; // Get the logged-in user's ID.

    // Fetch the user data from your data source (e.g., database).
    const user = await User.findById(userId);

    if (!user) {
      return res.status(404).send("User not found");
    }

    // Fetch the user's profile data (replace with your actual method to fetch the profile).
    const userProfile = await Profile.findOne({ user: userId }).exec();
    
    const plans = await Plan.find({}).exec();
    const wallets = await Wallet.find({}).exec();
    res.render(__dirname + "../../../views/user/add-fund", {
      data: plans,
      wallet: wallets,
      userProfile: userProfile,
    });
  } catch (err) {
    console.error(err);
    // Handle the error appropriately (e.g., send an error response).
  }
});

// app.get("/user/dashboard", (req, res) => {
//   res.render(__dirname + "../../..//views/user/dashboard");
// });

router.get("/user/fund-history", async (req, res) => {
  const userId = req.user.id; // Get the logged-in user's ID.

  // Fetch the user data from your data source (e.g., database).
  const user = await User.findById(userId);

  if (!user) {
    return res.status(404).send("User not found", {
      userProfile: userProfile,
    });
  }

  // Fetch the user's profile data (replace with your actual method to fetch the profile).
  const userProfile = await Profile.findOne({ user: userId }).exec();

  res.render(__dirname + "../../../views/user/fund-history", {
    userProfile: userProfile,
  });
});

router.get("/user/invest-history", async (req, res) => {
  const userId = req.user.id; // Get the logged-in user's ID.

  // Fetch the user data from your data source (e.g., database).
  const user = await User.findById(userId);

  if (!user) {
    return res.status(404).send("User not found", {
      userProfile: userProfile,
    });
  }

  // Fetch the user's profile data (replace with your actual method to fetch the profile).
  const userProfile = await Profile.findOne({ user: userId }).exec();

  res.render(__dirname + "../../../views/user/invest-history", {
    userProfile: userProfile,
  });
});

router.get("/user/money-transfer", (req, res) => {
  res.render(__dirname + "../../../views/user/money-transfer");
});

router.get("/user/payout-history", (req, res) => {
  res.render(__dirname + "../../../views/user/payout-history");
});

router.get("/user/payout", (req, res) => {
  res.render(__dirname + "../../../views/user/payout");
});

router.get("/user/profile", async (req, res) => {
  const userId = req.user.id; // Get the logged-in user's ID.

  // Fetch the user data from your data source (e.g., database).
  const user = await User.findById(userId);

  if (!user) {
    return res.status(404).send("User not found", {
      userProfile: userProfile,
    });
  }

  // Fetch the user's profile data (replace with your actual method to fetch the profile).
  const userProfile = await Profile.findOne({ user: userId }).exec();

  res.render(__dirname + "../../..//views/user/profile", {
    userProfile: userProfile,
  });
});

router.get("/user/referral", async (req, res) => {
  const userId = req.user.id; // Get the logged-in user's ID.

  // Fetch the user data from your data source (e.g., database).
  const user = await User.findById(userId);

  if (!user) {
    return res.status(404).send("User not found", {
      userProfile: userProfile,
    });
  }

  // Fetch the user's profile data (replace with your actual method to fetch the profile).
  const userProfile = await Profile.findOne({ user: userId }).exec();

  res.render(__dirname + "../../../views/user/referral", {
    userProfile: userProfile,
  });
});

router.get("/user/transaction", (req, res) => {
  res.render(__dirname + "../../../views/user/transaction");
});

router.get("/user/twostep-security", getTwoFactor);

module.exports = router;
