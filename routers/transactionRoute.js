const express = require("express");
const router = express.Router();
const upload = require("../config/multer");
const transactionController = require("../controllers/transactionController");

router.get("/transaction", transactionController.getTransaction);
router.get("/transaction", transactionController.getAllTransactions);
router.get("/transaction/:id", transactionController.getTransactionById);
router.delete("/transaction/:id", transactionController.deleteTransaction);
router.put(
  "/transaction/:id",
  upload.single("paymentProof"),
  transactionController.updateTransaction
);
router.post(
  "/transaction",
  upload.single("paymentProof"),
  transactionController.transactionWallet
);

module.exports = router;
