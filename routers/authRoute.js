const express = require("express");
const router = express.Router();
const authController = require("../controllers/authController");
const twoFactorController = require("../controllers/twoFactorController");

router.get("/login", authController.login_get);
router.post("/login", authController.login_post);
router.get("/registration", authController.signup_get);
router.post("/registration", authController.signup_post);
router.get("/forget-password", authController.forget_password)
router.post("/logout", authController.logout)

// 2 factor route
router.post("/user/twostep-security", twoFactorController.verifyTwoFactorToken);
router.get("/verification/:code", authController.verifyEmail)

router.get("/admin/login", authController.loginAdmin_get);
router.post("/admin/login", authController.loginAdmin_post);
router.get("/admin/register", authController.signupAdmin_get);
router.post("/admin/register", authController.signupAdmin_post);

module.exports = router;
