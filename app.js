require("dotenv").config();
const { ClientPromise } = require("./config/database");
const session = require("express-session");
const MongoStore = require("connect-mongo");

const express = require("express");
const app = express();

const path = require("path");
const cookieParser = require("cookie-parser");

const port = process.env.PORT || 3000;

const authRoute = require("./routers/authRoute");
const planRoute = require("./routers/planRoute");
const emailRoute = require("./routers/emailRoute");
const walletRoute = require("./routers/walletRoute");
const userRoute = require("./routers/web/userRoute");
const messageRoute = require("./routers/messageRoute");
const adminRoute = require("./routers/web/adminRoute");
const profileRoute = require("./routers/profileRoute");
const withdrawalRoute = require("./routers/withdrawalRoute");
const transactionRoute = require("./routers/transactionRoute");

// const authMiddleware = require('./middleware/auth')

const Plan = require("./models/Plan");

let sessionOptions = {
  secret: process.env.SESSION_SECRET,
  resave: false,
  saveUninitialized: false,
  store: MongoStore.create({ clientPromise: ClientPromise }),
  cookie: {
    maxAge: 3600000, // an hour
  },
};

if (process.env.PRODUCTION === "true") {
  sessionOptions.proxy = true;
  sessionOptions.cookie = {
    secure: true, // required for cookies to work on HTTPS
    httpOnly: true,
    sameSite: "none",
    maxAge: 3600000, // an hour
  };
}

// session setup
app.use(session(sessionOptions));

// register view engine
app.set("view engine", "ejs");

app.set("views", path.join(__dirname, "views", "pages"));

// middleware
app.use(express.json());
app.use(cookieParser());
app.use(express.static("public"));
app.use(express.urlencoded({ extended: true }));

// Start the server
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});

// pages general route
app.get("/", async (req, res) => {
  try {
    const plan = await Plan.find({}).exec();
    res.render(__dirname + "/views/index", { data: plan });
  } catch (err) {
    console.error(err);
    // Handle the error appropriately (e.g., send an error response).
    res.status(500).send("An error occurred");
  }
});

app.get("/about", (req, res) => {
  res.render(__dirname + "/views/pages/about");
});

app.get("/plan", (req, res) => {
  res.render(__dirname + "/views/pages/plan");
});

app.get("/blog", (req, res) => {
  res.render(__dirname + "/views/pages/blog");
});

app.get("/blog-details", (req, res) => {
  res.render(__dirname + "/views/pages/blog-details");
});

app.get("/error-404", (req, res) => {
  res.render(__dirname + "/views/pages/error-404");
});

app.get("/contact", (req, res) => {
  res.render(__dirname + "/views/pages/contact");
});

// routes
app.use(userRoute);
app.use(authRoute);
app.use(planRoute);
app.use(adminRoute);
app.use(emailRoute);
app.use(walletRoute);
app.use(messageRoute);
app.use(profileRoute);
app.use(withdrawalRoute);
app.use(transactionRoute);
