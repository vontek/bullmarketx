const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const emailSchema = new Schema(
  {
    email: {
      unique: true,
      type: String,
      required: true,
      trim: true,
    },
  },
  { timestamps: true }
);

const Email = mongoose.model("Email", emailSchema);

module.exports = Email;
