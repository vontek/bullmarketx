const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const transactionSchema = new Schema(
  {
    user: {
      type: Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    transactionId: {
      type: String,
      required: true,
    },
    transactionStatus: {
      type: String,
      enum: ["pending", "success"],
      default: "pending",
    },
    amountPaid: {
      type: Number,
      required: true,
    },
    walletType: {
      type: String,
      required: true,
    },
    purchasedPlan: {
      type: String,
      required: true,
    },
    paymentProof: {
      type: String,
      required: true,
    },
    cloudinary_id: {
      type: String,
      required: true,
    },
  },
  { timestamps: true }
);

const Transaction = mongoose.model("Transaction", transactionSchema);

module.exports = Transaction;
