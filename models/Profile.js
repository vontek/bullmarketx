const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const profileSchema = new Schema(
  {
    user: {
      type: Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    firstname: {
      type: String,
      required: true,
      trim: true,
    },
    lastname: {
      type: String,
      required: true,
      trim: true,
    },
    username: {
      unique: true,
      type: String,
      required: true,
      trim: true,
    },
    email: {
      unique: true,
      type: String,
      required: true,
      trim: true,
    },
    phone: {
      type: String,
      required: true,
    },
    profilePicture: {
      type: String,
    },
    cloudinary_id: {
      type: String,
      required: true,
    },
    country: {
      type: String,
      trim: true,
      required: true,
    },
    Balance: {
      type: String,
      default: 0,
    },
    transaction: [
      {
        type: Schema.Types.ObjectId,
        ref: "Transaction",
      },
    ],
    sex: {
      type: String,
      
    },
    address: {
      type: String,
      trim: true,
      required: true,
    },
  },
  { timestamps: true }
);

// // Set default avatars based on sex
// profileSchema.pre("save", function (next) {
//   if (!this.profilePicture) {
//     if (this.sex === "Male") {
//       this.profilePicture = "default_male_avatar.png";
//     } else if (this.sex === "Female") {
//       this.profilePicture = "default_female_avatar.png";
//     }
//   }
//   next();
// });

// Create the Profile model
const Profile = mongoose.model("Profile", profileSchema);

module.exports = Profile;
