const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const planSchema = new Schema(
  {
    title: {
      type: String,
      required: true,
    },
    interestRate: {
      type: Number,
      required: true,
    },
    duration: {
      type: String,
      required: true,
    },
    interest: {
      type: Number,
      required: true,
    },
    minimum_amount: {
      type: Number,
      required: true,
    },
    maximum_amount: {
      type: Number,
      required: true,
    },
  },
  { timestamps: true }
);

const Plan = mongoose.model("Plan", planSchema);

module.exports = Plan;
