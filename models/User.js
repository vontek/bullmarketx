const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const userSchema = new Schema(
    {
        fullname: {
            type: String,
            required: true,
            trim: true,
        },
        username: {
            unique: true,
            type: String,
            required: true,
            trim: true,
        },
        email: {
            unique: true,
            type: String,
            required: true,
            trim: true,
        },
        phone: {
            type: String,
            required: true,
            trim: true,
        },
        password: {
            unique: true,
            type: String,
            required: true,
        },
        role: {
            type: String,
            enum: ["user", "admin"],
            default: "user",
        },
        temp2FAToken: {
            type: String,
            trim: true,
        },
        enabled2FA: {
            type: Boolean,
        },
        isVerified: {
            type: Boolean,
        },
    },
    { timestamps: true },
);

const User = mongoose.model("User", userSchema);

module.exports = User;
