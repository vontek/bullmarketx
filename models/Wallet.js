const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const walletSchema = new Schema(
  {
    wallet_type: {
      type: String,
      unique: true,
      required: true,
    },
    wallet_address: {
      type: String,
      unique: true,
      required: true,
    },
    qr_code: {
      type: String,
    },
    cloudinary_id: {
      type: String,
    },
  },
  { timestamps: true }
);

const Wallet = mongoose.model("Wallet", walletSchema);

module.exports = Wallet;
