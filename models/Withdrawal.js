const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const withdrawalSchema = new Schema(
  {
    user: {
      type: Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    amount: {
      type: Number,
      required: true,
    },
    walletType: {
      type: String,
      required: true,
    },
    walletAddress: {
      type: String,
      required: true,
    },
    Status: {
      type: String,
      enum: ["pending", "success"],
      default: "pending",
    },
  },
  { timestamps: true }
);

const Withdrawal = mongoose.model("Withdrawal", withdrawalSchema);
module.exports = Withdrawal;
