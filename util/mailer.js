require("dotenv").config();
const VerificationCode = require("../models/VerificationCode");

const { transporterConfig } = require("../config/mailer");
const nodeMailer = require("nodemailer");
const { v4: uuidv4 } = require("uuid");

exports.sendConfirmationEmail = async (email, username) => {
  const code = uuidv4();
  console.log(transporterConfig.auth.pass);
  console.log(transporterConfig.auth.user);
  const productionMode = Boolean(process.env.PRODUCTION);
  try {
    const transporter = nodeMailer.createTransport(transporterConfig);
    const mailOptions = {
      //from: "noreply@bullmarkets.org",
      from: "support@bullmarkets.org",
      to: email,
      subject: "Thanks for Signing Up!",
      html: `
        <h1>Verify Your Email Address</h1>
        <h3>Hello ${username}</h3>
        <p>Thank you for signing up for our service. To complete your registration, please click the button below:</p>
        <a href="${
          productionMode ? "https://bullmarkets.org" : "http://localhost:5000"
        }/verification/${code}" style="background-color: #4CAF50; color: white; padding: 10px 20px; text-decoration: none; border-radius: 5px;">Verify Email</a>
        <p>If you didn't sign up for our service, you can safely ignore this email.</p>
            `,
    };

    await transporter.sendMail(mailOptions);
    let verificationCode = await VerificationCode.findOne({ email });
    if (!verificationCode) {
      verificationCode = new VerificationCode({ code, email });
    } else {
      verificationCode.code = code;
    }
    await verificationCode.save();
    console.log("Confirmation email sent");
  } catch (error) {
    console.log("Error sending email:", error);
  }
};
