const User = require("../models/User");

module.exports.verify2Factor = async (req, res, next) => {
    let path = `${req.baseUrl}${req.path}`;
    let user = await User.findById(req.user.id);
    if (user.enabled2FA && !req.session.isVerified && path !== "/user/twostep-security"){
        return res.redirect("/user/twostep-security");
    }
    next()
}