const jwt = require("jsonwebtoken");
const Admin = require("../models/Admin");

// Middleware to protect routes
module.exports.protectRoute = (req, res, next) => {
  // Get the token from the request header
  const token = req.header("x-auth-token");
  const sessionToken = req.session?.jwt;

  // Check if the token is missing
  if (!(token || sessionToken)) {
    return res.status(401).redirect("/login");
    // return res.status(401).json({ message: 'Authorization denied. Token missing.' });
  }

  try {
    // Verify the token
    const decoded = jwt.verify(token ?? sessionToken, "randomString");

    req.user = decoded.user;
    next();
  } catch (err) {
    console.log(req.accepts("html"));
    return res.status(401).redirect("/login");
    // res.status(401).json({ message: 'Authorization denied. Invalid token.' });
  }
};

// Middleware to protect admin routes
module.exports.protectAdminRoute = async (req, res, next) => {
  // Get the token from the request header
  const token = req.header("x-auth-token");
  const sessionToken = req.session?.jwt;
  let path = `${req.baseUrl}${req.path}`;

  if (
    path !== "/admin/login" &&
    path !== "/admin/register" &&
    path !== "/admin/forgot-password"
  ) {
    // Check if the token is missing
    if (!(token || sessionToken)) {
      return res.status(401).redirect("/admin/login");
      // return res.status(401).json({ message: 'Authorization denied. Token missing.' });
    }

    try {
      // Verify the token
      const decoded = jwt.verify(token ?? sessionToken, "randomString");

      const admin = await Admin.findById(decoded.user.id);
      if (!admin) {
        return res.status(401).redirect("/admin/login");
      }
      req.admin = admin;
      next();
    } catch (err) {
      console.log(req.accepts("html"));
      return res.status(401).redirect("/admin/login");
      // res.status(401).json({ message: 'Authorization denied. Invalid token.' });
    }
  }
  next();
};

// Middleware function for role-based authorization
module.exports.requireRole = (role) => {
  return async (req, res, next) => {
    const token = req.header("x-auth-token");
    const sessionToken = req.session?.jwt;

    if (!(token || sessionToken)) {
      return res.status(401).json({ message: "Access denied. Token missing." });
    }

    try {
      const decoded = jwt.verify(token ?? sessionToken, "randomString");
      const admin = await Admin.findById(decoded.user.id);

      if (!admin) {
        return res
          .status(401)
          .json({ message: "Access denied. Invalid user." });
      }

      // Add role-based authorization check here
      if (admin.role !== role) {
        return res
          .status(403)
          .json({ message: "Access denied. Insufficient permissions." });
      }

      req.admin = admin;
      next();
    } catch (err) {
      return res.status(401).json({ message: "Access denied. Invalid token." });
    }
  };
};
