const Profile = require("../models/Profile");

// Middleware to fetch and store userProfile
const fetchUserProfile = async (req, res, next) => {
  try {
    const userId = req.user.id; // Get the logged-in user's ID.

    // Fetch the user's profile data (replace with your actual method to fetch the profile).
    const userProfile = await Profile.findOne({ user: userId }).exec();

    // Make userProfile available to all routes by attaching it to the request object.
    req.userProfile = userProfile;
    next();
  } catch (err) {
    console.error(err);
    res.status(500).send("Internal Server Error");
  }
};

module.exports = fetchUserProfile;
