const upload = require("../config/multer");
const cloudinary = require("../config/cloudinary");
const path = require("path");
const User = require("../models/User");
const Profile = require("../models/Profile");
const Transaction = require("../models/Transaction");

exports.getProfile = (req, res) => {
  res.render(__dirname + "/views/user/profile");
};

// Controller for creating a new profile
exports.createProfile = async (req, res) => {
  try {
    // Log the request body
    console.log("req.body:", req.body);

    // Log the uploaded file information
    console.log("req.file:", req.file);

    // Upload image to Cloudinary using the req.file.path.
    const result = await cloudinary.uploader.upload(req.file.path);

    const user = await User.findOne(req.params.id);

    const transaction = await Transaction.findOne(req.params.id);

    console.log(user);

    console.log(transaction);

    const profile = new Profile({
      firstname: req.body.firstname,
      lastname: req.body.lastname,
      username: req.body.username,
      email: req.body.email,
      phone: req.body.phone,
      country: req.body.country,
      sex: req.body.sex,
      address: req.body.address,
      user: user,
      transaction: transaction,
      profilePicture: result.secure_url,
      cloudinary_id: result.public_id,
    });

    await profile.save();
    // res.status(201).json(profile);
    res.redirect("/user/profile");
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
};

// Controller for getting a profile by ID
exports.getProfileById = async (req, res) => {
  try {
    const profile = await Profile.findById(req.params.id);
    if (!profile) {
      return res.status(404).json({ error: "Profile not found" });
    }
    res.json(profile);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

// Controller for updating a profile by ID
exports.updateProfile = async (req, res) => {
  try {
    const profile = await Profile.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
    });
    if (!profile) {
      return res.status(404).json({ error: "Profile not found" });
    }
    res.json(profile);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

// Controller for deleting a profile by ID
exports.deleteProfile = async (req, res) => {
  try {
    const profile = await Profile.findByIdAndRemove(req.params.id);
    if (!profile) {
      return res.status(404).json({ error: "Profile not found" });
    }
    res.json({ message: "Profile deleted successfully" });
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};
