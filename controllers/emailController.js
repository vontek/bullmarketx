const Email = require("../models/Email");

module.exports.email_get = (req, res) => {
  res.render(__dirname + "/views/pages/index");
};

module.exports.email_post = async (req, res) => {
  const { email } = req.body;
  try {
    const newEmail = new Email({ email });
    await newEmail.save();
    // res.status(201).json({ Email: "Subscription successful" });
    res.redirect("/");
  } catch (error) {
    res.status(500).json({ error: "Subscription failed" });
  }
};

module.exports.get_email = (req, res) => {
  const id = req.params.id;
  Email.findById(id)
    .then((data) => {
      if (!data)
        res.status(404).send({ message: "Not found Email with id " + id });
      else res.send(data);
    })
    .catch((err) => {
      res.status(500).send({ message: "Error retrieving Email with id=" + id });
    });
};

module.exports.get_emails = (req, res) => {
  Email.find()
    .then((result) => {
      res.send(result);
    })
    .catch((err) => {
      console.log(err);
    });
};
