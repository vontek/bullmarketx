const upload = require("../config/multer");
const cloudinary = require("../config/cloudinary");
const path = require("path");
const Transaction = require("../models/Transaction");
const User = require("../models/User");

// Controller for handling wallet operations
const transactionController = {
  getTransaction: (req, res) => {
    res.render(__dirname + "/views/user/add-fund");
  },

  transactionWallet: async (req, res) => {
    try {
      // Log the request body
      console.log("req.body:", req.body);

      // Log the uploaded file information
      console.log("req.file:", req.file);

      // Upload image to Cloudinary using the req.file.path.
      const result = await cloudinary.uploader.upload(req.file.path);

      const user = await User.findOne(req.params.id);

      //console.log(user);

      // Create a new wallet document using data from the request and Cloudinary result.
      const transaction = new Transaction({
        transactionId: req.body.transactionId,
        amountPaid: req.body.amountPaid,
        walletType: req.body.walletType,
        purchasedPlan: req.body.purchasedPlan,
        user: user,
        paymentProof: result.secure_url,
        cloudinary_id: result.public_id,
      });

      // Save the wallet details in MongoDB.
      await transaction.save();

      // Send a successful response with the created wallet data.
      //   res.status(200).send({
      //     transaction,
      //   });
      res.redirect("/user/add-fund");
    } catch (err) {
      // Handle any errors by sending an error response and logging the error.
      console.error(err);
      res
        .status(500)
        .send({ error: "An error occurred while creating the transaction." });
    }
  },

  getAllTransactions: async (req, res) => {
    try {
      const transactions = await Transaction.find();
      res.status(200).json(transactions);
    } catch (err) {
      res.status(500).json({ error: err.message });
    }
  },

  getTransactionById: async (req, res) => {
    try {
      const transaction = await Transaction.findById(req.params.id);
      if (!transaction) {
        return res.status(404).json({ error: "Transaction not found" });
      }
      res.status(200).json(transaction);
    } catch (err) {
      res.status(500).json({ error: err.message });
    }
  },

  updateTransaction: async (req, res) => {
    try {
      const updatedTransaction = await Transaction.findByIdAndUpdate(
        req.params.id,
        req.body,
        { new: true }
      );
      if (!updatedTransaction) {
        return res.status(404).json({ error: "Transaction not found" });
      }
      res.status(200).json(updatedTransaction);
    } catch (err) {
      res.status(500).json({ error: err.message });
    }
  },

  deleteTransaction: async (req, res) => {
    try {
      const deletedTransaction = await Transaction.findByIdAndRemove(
        req.params.id
      );
      if (!deletedTransaction) {
        return res.status(404).json({ error: "Transaction not found" });
      }
      res.status(204).end();
    } catch (err) {
      res.status(500).json({ error: err.message });
    }
  },
};

module.exports = transactionController;
