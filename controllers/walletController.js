const Wallet = require("../models/Wallet");
const upload = require("../config/multer");
const cloudinary = require("../config/cloudinary");
const path = require("path");

// Controller for handling wallet operations
const walletController = {
  getWallet: (req, res) => {
    res.render(__dirname + "/views/admin/add-payment");
  },

  createWallet: async (req, res) => {
    try {
      // Log the request body
      console.log("req.body:", req.body);

      // Log the uploaded file information
      console.log("req.file:", req.file);

      // Upload image to Cloudinary using the req.file.path.
      const result = await cloudinary.uploader.upload(req.file.path);

      // Create a new wallet document using data from the request and Cloudinary result.
      const wallet = new Wallet({
        wallet_type: req.body.wallet_type,
        wallet_address: req.body.wallet_address,
        qr_code: result.secure_url,
        cloudinary_id: result.public_id,
      });

      // Save the wallet details in MongoDB.
      await wallet.save();

      // Send a successful response with the created wallet data.
      // res.status(200).send({
      //   wallet,
      // });
      res.redirect(__dirname + "/views/admin/add-payment");
    } catch (err) {
      // Handle any errors by sending an error response and logging the error.
      console.error(err);
      res
        .status(500)
        .send({ error: "An error occurred while creating the wallet." });
    }
  },

  getAllWallets: async (req, res) => {
    try {
      const wallets = await Wallet.find();
      res.status(200).json(wallets);
    } catch (err) {
      res.status(500).json({ error: err.message });
    }
  },

  getWalletById: async (req, res) => {
    try {
      const wallet = await Wallet.findById(req.params.id);
      if (!wallet) {
        return res.status(404).json({ error: "Wallet not found" });
      }
      res.status(200).json(wallet);
    } catch (err) {
      res.status(500).json({ error: err.message });
    }
  },

  updateWallet: async (req, res) => {
    try {
      const wallet = await Wallet.findByIdAndUpdate(req.params.id, req.body, {
        new: true,
      });
      if (!wallet) {
        return res.status(404).json({ error: "Wallet not found" });
      }
      res.status(200).json(wallet);
    } catch (err) {
      res.status(500).json({ error: err.message });
    }
  },

  deleteWallet: async (req, res) => {
    try {
      const wallet = await Wallet.findByIdAndRemove(req.params.id);
      if (!wallet) {
        return res.status(404).json({ error: "Wallet not found" });
      }
      res.status(204).end();
    } catch (err) {
      res.status(500).json({ error: err.message });
    }
  },
};

module.exports = walletController;
