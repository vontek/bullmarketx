const speakeasy = require("speakeasy");
const qrcode = require("qrcode");
const User = require("../models/User");



const getTwoFactor = (req, res) => {
    const secret = speakeasy.generateSecret({
        name: "Bullmarketx"
    })
    qrcode.toDataURL(secret.otpauth_url, function (err, data){
        if (data){
            res.render("../../views/user/twostep-security", {qrData: data, secret: secret.base32});
        } else {
            res.render("../../views/pages/error-404")
        }
    })
}

const verifyTwoFactorToken = async (req, res) => {
    const {code, secret} = req.body
    let user = await User.findById(req.user.id);
    // if user has enabled 2fa then get the temporary key
    // then use it to verify the current code
    // else generate a new secret and store it
    // for the user then carry the person to the dashboard
    if (user.enabled2FA){
        const isVerified = speakeasy.totp.verify({
            secret: user.temp2FAToken,
            encoding: "base32",
            token: code,
            window: 3
        })
        if (isVerified){
            req.session.isVerified = true;
            req.session.save()
            return res.redirect('/dashboard')
        } else {
            return res.redirect('/user/twostep-security')
        }
    } else {
        if (code.length > 0){
            const isVerified = speakeasy.totp.verify({
                secret: secret,
                encoding: "base32",
                token: code,
                window: 3
            })
            if (isVerified) {
                req.session.isVerified = true;
                req.session.save()
                user.temp2FAToken = secret
                user.enabled2FA = true
                await user.save()
                return res.redirect('/dashboard')
            }
        }
        return res.redirect('/user/twostep-security')
    }

}


module.exports = {getTwoFactor, verifyTwoFactorToken}