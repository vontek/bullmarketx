const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const User = require("../models/User");
const Admin = require("../models/Admin");
const { validationResult } = require("express-validator");
const { sendConfirmationEmail } = require("../util/mailer");
const VerificationCode = require("../models/VerificationCode");


module.exports.forget_password = (req, res) => {
  res.render("forget-password");
};

module.exports.signup_get = (req, res) => {
  res.render("registration");
};

module.exports.signupAdmin_get = (req, res) => {
  res.render("../../views/admin/register");
};

module.exports.signupAdmin_post = async (req, res) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        errors: errors.array(),
      });
    }

    const { username, email, password } = req.body;

    const emailLowerCase = email.toLowerCase();

    let admin = await Admin.findOne({ email: emailLowerCase });

    if (admin) {
      return res.status(400).json({
        msg: "User Already Exists",
      });
    }

    const usernameLowerCase = username ? username.toLowerCase() : null;

    if (usernameLowerCase) {
      admin = await Admin.findOne({ username: usernameLowerCase });

      if (admin) {
        return res.status(400).json({
          msg: "Username Already Exists",
        });
      }
    }

    admin = new Admin({
      username: usernameLowerCase,
      email: emailLowerCase,
      password,
    });

    const salt = await bcrypt.genSalt(10);
    admin.password = await bcrypt.hash(password, salt);

    await admin.save();

    const payload = {
      user: {
        id: admin.id,
      },
    };

    jwt.sign(
      payload,
      "randomString",
      {
        expiresIn: 10000,
      },
      (err, token) => {
        if (err) throw err;
        // res.status(200).json({
        //   token,
        // });
        res.redirect("/admin/login");
      }
    );
  } catch (error) {
    console.log(error);
    res.status(500).send("Error in Saving");
  }
};

module.exports.signup_post = async (req, res) => {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({
        errors: errors.array(),
      });
    }

    const { fullname, username, email, phone, password } = req.body;

    const emailLowerCase = email.toLowerCase();

    let user = await User.findOne({ email: emailLowerCase });

    if (user) {
      return res.status(400).json({
        msg: "User Already Exists",
      });
    }

    const usernameLowerCase = username ? username.toLowerCase() : null;

    if (usernameLowerCase) {
      user = await User.findOne({ username: usernameLowerCase });

      if (user) {
        return res.status(400).json({
          msg: "Username Already Exists",
        });
      }
    }

    user = new User({
      fullname,
      username: usernameLowerCase,
      email: emailLowerCase,
      phone,
      password,
    });

    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(password, salt);

    await user.save();

    await sendConfirmationEmail(email, username);

    const payload = {
      user: {
        id: user.id,
      },
    };

    jwt.sign(
      payload,
      "randomString",
      {
        expiresIn: 10000,
      },
      (err, token) => {
        if (err) throw err;
        // res.status(200).json({
        //   token,
        // });z

        res.render("check_registration", { validity: true })
        /* res.send('<script>alert("Check Your mail for verification")</script>')
         res.redirect("/login");*/
      }
    );
  } catch (error) {
    console.log(error);
    res.status(500).send("Error in Saving");
  }
};

module.exports.login_get = (req, res) => {
  res.render("login");
};

module.exports.loginAdmin_get = (req, res) => {
  res.render("../../views/admin/login");
};

module.exports.login_post = async (req, res) => {
  try {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(400).json({
        errors: errors.array(),
      });
    }

    const { identifier, password } = req.body;

    const identifierLowerCase = identifier.toLowerCase();

    let user = await User.findOne({
      $or: [{ email: identifierLowerCase }, { username: identifierLowerCase }],
    });

    if (!user) {
      return res.status(400).json({
        message: "User Not Exist",
      });
    }
    if (!user.isVerified) {
      return res.redirect('/')
    }

    const isMatch = await bcrypt.compare(password, user.password);

    if (!isMatch) {
      return res.status(400).json({
        message: "Incorrect Password!",
      });
    }

    const payload = {
      user: {
        id: user.id,
      },
    };

    jwt.sign(
      payload,
      "randomString",
      {
        expiresIn: 3600,
      },
      (err, token) => {
        // if (err) throw err;
        // res.status(200).json({
        //   token,
        // });

        req.session.jwt = token
        req.session.save(function (err) {
        })
        res.redirect("/user/dashboard");
      }
    );
  } catch (e) {
    console.error(e);
    res.status(500).json({
      message: "Server Error",
    });
  }
};

module.exports.loginAdmin_post = async (req, res) => {
  try {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(400).json({
        errors: errors.array(),
      });
    }

    const { identifier, password } = req.body;

    const identifierLowerCase = identifier.toLowerCase();

    let admin = await Admin.findOne({
      $or: [{ email: identifierLowerCase }, { username: identifierLowerCase }],
    });

    if (!admin) {
      return res.status(400).json({
        message: "User Not Exist",
      });
    }

    const isMatch = await bcrypt.compare(password, admin.password);

    if (!isMatch) {
      return res.status(400).json({
        message: "Incorrect Password!",
      });
    }

    const payload = {
      user: {
        id: admin.id,
      },
    };

    jwt.sign(
      payload,
      "randomString",
      {
        expiresIn: 3600,
      },
      (err, token) => {
        // if (err) throw err;
        // res.status(200).json({
        //   token,
        // });

        req.session.jwt = token;
        req.session.save();
        res.redirect("/admin/dashboard");
      }
    );
  } catch (e) {
    console.error(e);
    res.status(500).json({
      message: "Server Error",
    });
  }
};


// module.exports.logout = (req, res) => {
//   req.session.destroy(function (err) {
//     console.log(err)
//   });
//   res.status(204).redirect('/login');
// }

module.exports.logout = (req, res) => {
  req.session.destroy((err) => {
    if (err) {
      console.error('Error destroying session:', err);
      res.status(500).send('Internal Server Error');
    } else {
      res.redirect('/login');
    }
  });
};


module.exports.verifyEmail = async (req, res) => {
  const code = req.params.code;
  console.log(code)
  // const verificationCode = await VerificationCode.findOne({code})
  // if (verificationCode){
  //   const user = await User.findOne({email: verificationCode.email});
  //   user.isVerified = true;
  //   await user.save();
  //   await VerificationCode.deleteOne({code})
  //   return res.redirect('/login');
  // }
  // return res.redirect('/');
  try {
    const verificationCode = await VerificationCode.findOne({ code });

    if (!verificationCode) {
      throw new Error('Verification code not found');
    }

    const user = await User.findOne({ email: verificationCode.email });

    if (!user) {
      throw new Error('User not found');
    }

    user.isVerified = true;
    await user.save();
    await VerificationCode.deleteOne({ code });

    return res.redirect('/login');
  } catch (error) {
    console.error(error);
    return res.redirect('/');
  }
}