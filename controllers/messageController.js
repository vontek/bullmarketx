const Message = require("../models/Message");

module.exports.get_message = (req, res) => {
  const id = req.params.id;
  Message.findById(id)
    .then((data) => {
      if (!data)
        res.status(404).send({ message: "Not found Message with id " + id });
      else res.send(data);
    })
    .catch((err) => {
      res
        .status(500)
        .send({ message: "Error retrieving Message with id=" + id });
    });
};

module.exports.get_messages = (req, res) => {
  Message.find()
    .then((result) => {
      res.send(result);
    })
    .catch((err) => {
      console.log(err);
    });
};

module.exports.post_message = (req, res) => {
  // Validate request
  if (!req.body.message) {
    res.status(400).send({ message: "Content can not be empty!" });
    return;
  }
  // Create a message
  const message = new Message({
    fullname: req.body.fullname,
    email: req.body.email,
    message: req.body.message ? req.body.message : false,
  });

  // Save message in the database
  message
    .save(message)
    .then((data) => {
      // res.send(data);
      res.redirect("/contact");
    })
    .catch((err) => {
      console.log(err);
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the message.",
      });
    });
};

module.exports.update_message = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!",
    });
  }

  const id = req.params.id;

  Message.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (!data) {
        res.status(404).send({
          message: `Cannot update Message with id=${id}. Maybe Message was not found!`,
        });
      } else res.send({ message: "Message was updated successfully." });
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error updating Message with id=" + id,
      });
    });
};

module.exports.delete_message = (req, res) => {
  const id = req.params.id;

  Message.findByIdAndRemove(id)
    .then((data) => {
      if (!data) {
        res.status(404).send({
          message: `Cannot delete Message with id=${id}. Maybe Message was not found!`,
        });
      } else {
        res.send({
          message: "Message was deleted successfully!",
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Could not delete Message with id=" + id,
      });
    });
};

module.exports.delete_messages = (req, res) => {
  Message.deleteMany({})
    .then((data) => {
      res.send({
        message: `${data.deletedCount} Messages were deleted successfully!`,
      });
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all Messages.",
      });
    });
};
