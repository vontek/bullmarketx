const User = require("../models/User");
const Withdrawal = require("../models/Withdrawal");

// Create a function to handle creating a new withdrawal
exports.createWithdrawal = async (req, res) => {
  try {
    const user = await User.findOne(req.params.id);
    const { amount, walletType, walletAddress } = req.body;
    const withdrawal = new Withdrawal({
      amount,
      walletType,
      walletAddress,
      user,
    });
    const result = await withdrawal.save();
    // res.status(201).json(result);
    res.redirect("/user/dashboard");
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: error.message });
  }
};

// Create a function to retrieve a list of all withdrawals
exports.getAllWithdrawals = async (req, res) => {
  try {
    const withdrawals = await Withdrawal.find();
    res.status(200).json(withdrawals);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};
