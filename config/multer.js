require("dotenv").config();

const multer = require("multer");
const path = require("path");

// Multer config
const upload = multer({
  storage: multer.diskStorage({
    filename: function (req, file, callback) {
      callback(null, Date.now() + file.originalname);
    },
  }),
  fileFilter: (req, file, cb) => {
    let ext = path.extname(file.originalname);
    if (ext !== ".jpg" && ext !== ".jpeg" && ext !== ".png") {
      cb(new Error("Unsupported file type!"), false);
      return;
    }
    cb(null, true);
  },
});

module.exports = upload;
