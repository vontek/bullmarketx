require("dotenv").config();

const mongoose = require("mongoose");

const url = process.env.DB_URL;

const connectionParams = {
  useNewUrlParser: true,
  // useCreateIndex: true,
  useUnifiedTopology: true,
};
const ClientPromise = mongoose
  .connect(url, connectionParams)
  .then((m) => {
    console.log("Connected to the database ");
    return m.connection.getClient();
  })
  .catch((err) => {
    console.error(`Error connecting to the database. n${err}`);
  });

// const db_url = process.env.DB_URL;
// mongoose
//   .connect(
//     db_url,
//     { useNewUrlParser: true },
//     { useCreateIndex: true },
//     { useUnifiedTopology: true }
//   )
//   .then(() => {
//     console.log("connected to database");
//   })
//   .catch((error) => {
//     console.log(error);
//     console.log("error connecting to database");
//   });

module.exports = {ClientPromise}