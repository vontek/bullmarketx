require("dotenv").config();
const nodeMailer = require("nodemailer");

const auth = {
  user: process.env.MAIL_USERNAME,
  pass: process.env.MAIL_PASSWORD,
};

const transporterConfig =
  process.env.MAIL_SERVICE !== "gmail"
    ? {
        host: process.env.MAIL_HOST,
        port: process.env.MAIL_PORT,
        secure: Boolean(process.env.MAIL_SECURE),
        auth,
        // comment the values below if you do not want logs
        logger: true,
        transactionLog: true,
      }
    : {
        service: "gmail",
        auth,
        logger: true,
        transactionLog: true,
      };

module.exports = { transporterConfig };
